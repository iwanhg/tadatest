var Emitter = require('events');
var prompt = new Emitter();
var current = null;
var result = {};
process.stdin.resume();

process.stdin.on('data', function(data){
  prompt.emit(current, data.toString().trim());
});

prompt.on(':new', function(word1, question){
  current = word1;
  console.log(question);
  process.stdout.write('> ');
});

prompt.on(':end', function(){
  //main process
  isAnagram = result.word1.split("").sort().join("").replace(/ /g,"") === result.word2.split("").sort().join("").replace(/ /g,"")
  if(isAnagram) {
	  console.log('\n', "Words is Anagram");
  } else {
	  console.log('\n', "Words is Not Anagram");
  }
  process.stdin.pause();
});

prompt.emit(':new', 'word1', 'Insert Word 1?');

prompt.on('word1', function(data){
  result.word1 = data;
  prompt.emit(':new', 'word2', 'Insert Word 2');
});

prompt.on('word2', function(data){
  result.word2 = data;
  prompt.emit(':end');
});

