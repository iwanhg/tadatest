var Emitter = require('events');
var prompt = new Emitter();
var current = null;
var result = {};
process.stdin.resume();

process.stdin.on('data', function(data){
  prompt.emit(current, data.toString().trim());
});

prompt.on(':new', function(mon, question){
  current = mon;
  console.log(question);
  process.stdout.write('> ');
});

prompt.on(':end', function(){
  //main process
  days = new Date(result.yer, result.mon, 0).getDate();
  
  console.log('\n', days+" days");
  process.stdin.pause();
});

prompt.emit(':new', 'mon', 'Insert Month? (1,2,3,4 ... 12)');

prompt.on('mon', function(data){
  result.mon = data;
  prompt.emit(':new', 'yer', 'Insert Year?');
});

prompt.on('yer', function(data){
  result.yer = data;
  prompt.emit(':end');
});

