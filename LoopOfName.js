var Emitter = require('events');
var prompt = new Emitter();
var current = null;
var result = {};
process.stdin.resume();

process.stdin.on('data', function(data){
  prompt.emit(current, data.toString().trim());
});

prompt.on(':new', function(name, question){
  current = name;
  console.log(question);
  process.stdout.write('> ');
});

prompt.on(':end', function(){
  //main process
    
  var myString = result.name;
  var withoutSpace = myString.replace(/ /g,"");
  var length = withoutSpace.length;

  console.log("total character of '"+myString+"' without space is "+length);
  
  for(i=1;i<=length;i++) {
	console.log(i+". "+myString);
  }

  process.stdin.pause();
});

prompt.emit(':new', 'name', 'Insert Your Name?');

prompt.on('name', function(data){
  result.name = data;
  prompt.emit(':end');
});
